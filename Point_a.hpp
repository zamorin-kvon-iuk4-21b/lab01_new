#pragma once
#include <iostream>
#include <iomanip>
using namespace std;
void AddValueOfSideDiagonalElement(int matrix[MATRIX_SIZE][MATRIX_SIZE], int SumOfNumbers)
{
    for (int i = 0; i < MATRIX_SIZE; i++)
    {
        for (int j = 0; j < MATRIX_SIZE; j++)
        {
            if (i + j == MATRIX_SIZE - 1)
            {
                SumOfNumbers = 0;
                SumOfNumbers += (abs(matrix[i][j] / 10) + abs(matrix[i][j] % 10));
            }
        }
        for (int j = 0; j < MATRIX_SIZE; j++)
        {
            matrix[i][j] += SumOfNumbers;
        }
    }
}