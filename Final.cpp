#include "Matrix_Create.hpp"
#include "Point_a.hpp"
#include "Point_b.hpp"
#include "Point_c.hpp"
bool counter = false;
bool MenuChecker = true;
int PointMenuChecker = 0;
void OutMenu()
{
    system("cls");
    cout << "1. Matrix generation/regeneration" << endl;
    cout << "2. Add value of side diagonal element to each element of matrix" << endl;
    cout << "3. Check string for alternating signs" << endl;
    cout << "4. Look for the first null element" << endl;
    cout << "0. Exit" << endl;
}
void ContinueOrNot()
{
    int ContinueChecker = 1;
    cout << "Back to menu or not? 1-Yes 0-No" << endl;
    cin >> ContinueChecker;
    if (ContinueChecker == 0)
    {
        MenuChecker = false;
    }
}
int main()
{
    srand(time(nullptr));
    int matrix[MATRIX_SIZE][MATRIX_SIZE];
    int SumOfNumbers = 0;
    int stringNumber = 0;
    while (MenuChecker == true)
    {
        OutMenu();
        cin >> PointMenuChecker;
        switch (PointMenuChecker)
        {
        case 1:
            MatrixCreation(matrix);
            MatrixOut(matrix);
            ContinueOrNot();
            break;
        case 2:
            cout << "Add value of side diagonal element to each element of matrix:" << endl;
            AddValueOfSideDiagonalElement(matrix, SumOfNumbers);
            MatrixOut(matrix);
            ContinueOrNot();
            break;
        case 3:
            cout << "Check string for alternating signs: " << endl;
            cout << boolalpha << CheckForAlternatingSigns(matrix, stringNumber, counter) << endl;
            ContinueOrNot();
            break;
        case 4:
            cout << "Look for the first null element: " << endl;
            MatrixOut(matrix);
            FirstNullValue(matrix);
            ContinueOrNot();
            break;
        case 0:
            MenuChecker = false;
            break;
        }
    }
}