#pragma once
#include <iostream>
#include <iomanip>
using namespace std;
void FirstNullValue(int matrix[MATRIX_SIZE][MATRIX_SIZE])
{
    bool flag{ true };
    int percent{};
    percent = MATRIX_SIZE / 3;
    int columnNumber{};
    int stringNumber{};
    for (int i{ percent }; i < MATRIX_SIZE; i++)
    {
        if (!flag)
            break;
        for (int j{}; j < MATRIX_SIZE; j++)
        {
            if (!flag)
                break;
            if (matrix[i][j] == 0)
            {
                columnNumber = j + 1;
                stringNumber = i + 1;
                flag = false;
            }
        }
    }
    cout << "String = " << stringNumber << "\nColumn = " << columnNumber << '\n';
}