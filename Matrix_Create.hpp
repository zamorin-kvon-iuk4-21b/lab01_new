#pragma once
#include <iostream>
#include <iomanip>
using namespace std;
const int MATRIX_SIZE = 17;
const int MAX_RANDOM_VALUE = 25;
const int MIN_RANDOM_VALUE = 12;
void MatrixCreation(int matrix[MATRIX_SIZE][MATRIX_SIZE])
{
    for (int i = 0; i < MATRIX_SIZE; i++)
    {
        for (int j = 0; j < MATRIX_SIZE; j++)
        {
            matrix[i][j] = rand() % MAX_RANDOM_VALUE - MIN_RANDOM_VALUE;
        }
    }
}
void MatrixOut(int matrix[MATRIX_SIZE][MATRIX_SIZE])
{
    for (int i = 0; i < MATRIX_SIZE; i++)
    {
        for (int j = 0; j < MATRIX_SIZE; j++)
        {
            cout << setw(4) << matrix[i][j];
        }
        cout << endl;
    }
}
